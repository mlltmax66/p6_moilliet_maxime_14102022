import React, {useEffect, useState} from 'react';
import {Swiper, SwiperSlide} from "swiper/react";
import {Pagination, Navigation} from "swiper";

import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";

export default function (props) {
    const [videos, setVideos] = useState([])
    const [images, setImages] = useState([])
    const [show, setShow] = useState(false)

    useEffect(() => {
        fetchImagesVideos()
    }, [])

    const fetchImagesVideos = async () => {
        const data = await fetch('/api/tricks-images-videos/' + props.slug).then(res => res.json())
        setImages(data.images)
        setVideos(data.videos)
    }

    return <section className="py-8">
        <div className="flex justify-center sm:hidden">
            <button className="btn-primary w-full py-3" onClick={() => setShow(!show)}>
                {!show ? 'Voir plus' : 'Voir moins'}
            </button>
        </div>
        <div className={`py-8 ${!show ? 'hidden' : 'block'} sm:block`}>
            {images?.length || videos?.length ? <h2 className="text-3xl font-bold pb-6">Images et Vidéos</h2> : ''}
            <Swiper
                pagination={true}
                navigation={true}
                modules={[Pagination, Navigation]}
                className="mySwiper rounded-xl select-none"
            >
                {images.map((image, idx) => (
                    <SwiperSlide key={idx}>
                        <img className="w-full h-[300px] sm:h-[500px]" src={'../uploads/tricks/' + image.image.filename}
                             alt={'Image du trick numéro' + props.trickName}/>
                    </SwiperSlide>
                ))}
                {videos.map((video, idx) => (
                    <SwiperSlide key={idx}>
                        <iframe className="w-full h-[300px] sm:h-[500px]" src={video.video.source}
                                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                allowFullScreen></iframe>
                    </SwiperSlide>
                ))}
            </Swiper>
        </div>
    </section>
}
