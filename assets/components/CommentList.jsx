import React, {useEffect, useState} from 'react';
import {useInView} from "react-intersection-observer";

export default function ({trickId}) {
    const [comments, setComments] = useState([])
    const [total, setTotal] = useState(0)
    const [offset, setOffset] = useState(0)
    const {ref, inView} = useInView()

    useEffect(() => {
        if (inView && total !== comments.length || !comments.length) {
            fetchComments()
        }
    }, [inView])

    const fetchComments = async () => {
        const data = await fetch('/api/comments/' + trickId + '/' + offset).then(res => res.json())
        setComments([...comments, ...data.comments])
        setTotal(data.total)
        setOffset(offset + data.comments.length)
    }

    return (<section className="w-full mb-16">
        <div className="flex flex-col gap-6">
            {total !== 0 && comments.map((comment, idx) => (
                <Comment key={idx} comment={comment.comment}/>
            ))}
        </div>
        <span style={{visibility: 'hidden', width: '100%', display: 'block'}} ref={ref}>
            Intersection observer marker
        </span>
    </section>)
}

const Comment = ({comment}) => {
    const {user} = comment

    return (
        <article className="w-full bg-white rounded-xl border border-gray-200 shadow-sm overflow-hidden">
            <div className="flex items-center gap-3 py-3 bg-blue-50 px-4">
                <span className="h-12 w-12 rounded-full bg-blue-500 font-semibold text-white relative overflow-hidden">
                    <img src={'../uploads/users/' + user.thumbnail} alt=""/>
                </span>
                <p className="font-semibold">{user.username}</p>
            </div>
            <div className="py-8 flex gap-16 justify-between px-4">
                <p>{comment.content}</p>
                <span>{new Date(comment.createdAt).toLocaleDateString('fr')}</span>
            </div>
        </article>
    );
}