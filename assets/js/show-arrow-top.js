const arrowTop = document.querySelector('.arrow-top')

window.addEventListener('scroll', () => {
    if (window.scrollY > 950) {
        arrowTop.style.display = 'flex'
    } else {
        arrowTop.style.display = 'none'
    }
})