const cardTricks = Array.from(document.querySelectorAll('.trick'))
const showMoreButton = document.querySelector('#show-more')
const baseOffset = 6;
let offset = baseOffset;

let cardTricksSliced = cardTricks.slice(0, offset);
cardTricks.map(card => card.style.display = 'none')
cardTricksSliced.map(card => card.style.display = 'block')

showMoreButton.addEventListener('click', () => {
    cardTricksSliced = cardTricks.slice(0, offset + baseOffset);
    offset += baseOffset
    cardTricks.map(card => card.style.display = 'none')
    cardTricksSliced.map(card => card.style.display = 'block')

    if(offset >= cardTricks.length) {
        showMoreButton.style.display = 'none'
    }
})
