const openModals = Array.from(document.querySelectorAll('.modal-open'))
let trickId = null;

openModals.map((openModal) => openModal.addEventListener('click', (event) => {
    event.preventDefault()
    trickId = openModal.dataset.trick
    toggleModal()
}))

const overlay = document.querySelector('.modal-overlay')
overlay.addEventListener('click', toggleModal)

const closeModals = Array.from(document.querySelectorAll('.modal-close'))
closeModals.map((closeModal) =>
    closeModal.addEventListener('click', toggleModal)
)

document.onkeydown = function (evt) {
    let isEscape = false
    if ("key" in evt) {
        isEscape = (evt.key === "Escape" || evt.key === "Esc")
    }
    if (isEscape && document.body.classList.contains('modal-active')) {
        toggleModal()
    }
};

function toggleModal() {
    const body = document.querySelector('body')
    const modal = document.querySelector('.modal')
    modal.classList.toggle('opacity-0')
    modal.classList.toggle('pointer-events-none')
    body.classList.toggle('modal-active')

    const formModal = document.querySelector('.modal-form')
    formModal.setAttribute('action', '/supprimer-un-trick/' + trickId)
}