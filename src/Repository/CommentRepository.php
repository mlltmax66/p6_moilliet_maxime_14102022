<?php

namespace App\Repository;

use App\Entity\Comment;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Comment>
 *
 * @method Comment|null find($id, $lockMode = null, $lockVersion = null)
 * @method Comment|null findOneBy(array $criteria, array $orderBy = null)
 * @method Comment[]    findAll()
 * @method Comment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CommentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Comment::class);
    }

    public function save(Comment $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Comment $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function getComments(int $trickId, ?int $offset = 0, int $limit = 10): array
    {
        $q = $this->createQueryBuilder('c');
        $q->select(['c as comment', 'u as user'])
            ->join('c.user', 'u')
            ->join('c.trick', 't')
            ->where('t.id =' . $trickId)
            ->orderBy('c.createdAt', 'desc')
            ->setFirstResult($offset)
            ->setMaxResults($limit);

        return $q->getQuery()->getArrayResult();
    }

    /**
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function getCountComments(int $trickId)
    {
        $q = $this->createQueryBuilder('c');
        $q->select($q->expr()->count('c.id'))
            ->join('c.trick', 't')
            ->where('t.id =' . $trickId);

        return $q->getQuery()->getSingleScalarResult();
    }
}
