<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Trick;

class TrickFixtures extends Fixture implements DependentFixtureInterface
{
    private array $tricksDemoNames = [
        'Mute',
        'Indy',
        '360',
        '720',
        'Backflip',
        'Tail slide',
        'Method Air',
        'Backside Air',
        'Sad Grab',
        'Truck Driver',
    ];

    private array $tricksDemoSlugs = [
        'mute',
        'indy',
        '360',
        '720',
        'backflip',
        'tail-slide',
        'method-air',
        'backside-air',
        'sad-grab',
        'truck-driver',
    ];

    private array $content = [
        'Saisie de la carre frontside de la planche entre les deux pieds avec la main avant',
        'Saisie de la carre frontside de la planche, entre les deux pieds, avec la main arrière',
        'Saisie de la carre frontside de la planche entre les deux pieds avec la main avant',
        'Saisie de la carre backside de la planche entre les deux pieds avec la main arrière',
        'Saisie de la partie arrière de la planche, avec la main arrière',
        'Saisie de la partie avant de la planche, avec la main avant',
        'Saisie de la carre backside de la planche entre les deux pieds avec la main arrière',
        'Saisie de la carre frontside de la planche, entre les deux pieds, avec la main arrière',
        'Saisie de la carre backside de la planche, entre les deux pieds, avec la main avant',
        'Saisie du carre avant et carre arrière avec chaque main (comme tenir un volant de voiture)',
    ];

    private array $mainImages = [
        'mute.jpg',
        'Indy.jpg',
        '360.jpg',
        '720.jpg',
        'backflip.jpg',
        'tailslide.jpg',
        'methodair.jpg',
        'backsideair.jpg',
        'sad.jpg',
        'truckdriver.jpg',
    ];

    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i <= 9; $i++) {
            $trick = new Trick();
            $trick->setTitle($this->tricksDemoNames[$i])
                ->setContent($this->content[$i])
                ->setSlug($this->tricksDemoSlugs[$i])
                ->setMainImage($this->mainImages[$i])
                ->setCategory($this->getReference(Category::class . '_' . rand(0, 4)))
                ->setUser($this->getReference(User::class . '_' . rand(0, 9)));

            $manager->persist($trick);

            $this->addReference(Trick::class . '_' . $i, $trick);
        }
        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [CategoryFixtures::class, UserFixtures::class];
    }


}
