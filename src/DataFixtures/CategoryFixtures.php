<?php

namespace App\DataFixtures;

use App\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class CategoryFixtures extends Fixture
{
    private array $categoriesDemoName = ['Grabs', 'Rotations', 'Flips', 'Slides', 'One foot'];

    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i <= 4; $i++) {
            $category = new Category();
            $category->setName($this->categoriesDemoName[$i]);

            $manager->persist($category);

            $this->addReference(Category::class . '_' . $i, $category);
        }

        $manager->flush();
    }
}
