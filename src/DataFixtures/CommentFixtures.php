<?php

namespace App\DataFixtures;

use App\Entity\Comment;
use App\Entity\Trick;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class CommentFixtures extends Fixture implements DependentFixtureInterface
{
    private array $comments = [
        'Très difficile à réaliser!',
        'Figure très dangereuse mais magnifique à voir!!',
        'Super',
        'Génial',
        'Je commence à la maitriser',
        'Merci pour ce site. J\'ai enfin les informations intéressantes concernant le monde du snowboard',
        'Merci pour les infos, il ne reste plus qu\'à pratiquer',
        'Figure trop difficile pour un débutant',
        'C\'est la figure de Tony Hawk!!',
        'Quelqu\'un pourrait expliquer la figure en détail?',
    ];

    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < 10; $i++) {
            $comment = new Comment();
            $comment->setContent($this->comments[$i])
                ->setUser($this->getReference(User::class . '_' . rand(0, 9)))
                ->setTrick($this->getReference(Trick::class . '_' . rand(0, 9)));

            $manager->persist($comment);
        }

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [UserFixtures::class, TrickFixtures::class];
    }
}
