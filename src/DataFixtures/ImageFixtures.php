<?php

namespace App\DataFixtures;

use App\Entity\Image;
use App\Entity\Trick;
use App\Services\UploadHelper;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class ImageFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i <= 25; $i++) {
            $image = new Image();
            $image->setFilename(($i + 1) . '.jpg')
                ->setTrick($this->getReference(Trick::class . '_' . rand(0, 9)));

            $manager->persist($image);

            $this->addReference(Image::class . '_' . $i, $image);
        }

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [TrickFixtures::class];
    }
}
