<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class UserFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i <= 9; $i++) {
            $user = new User();
            $user->setEmail('email@email.com' . $i)
                ->setUsername('name' . $i)
                ->setPlainPassword('password')
                ->setThumbnail(($i + 1) . '.jpg')
                ->setRoles(['ROLE_USER_VERIFY']);

            $manager->persist($user);

            $this->addReference(User::class . '_' . $i, $user);
        }

        $manager->flush();
    }
}

