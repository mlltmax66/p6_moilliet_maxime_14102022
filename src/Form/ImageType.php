<?php

namespace App\Form;

use App\Entity\Image;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\UX\Dropzone\Form\DropzoneType;

class ImageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->addEventListener(FormEvents::POST_SET_DATA, function (FormEvent $event) {
            $form = $event->getForm();
            $image = $form->getData();
            $form->add('file', DropzoneType::class, [
                'attr' => [
                    'placeholder' => 'Glisser ou déposer une image',
                    'class' => 'bg-white'
                ],
                'label' => 'Image',
                'label_attr' => [
                    'class' => 'label'
                ],
                'required' => !($image instanceof Image && ($image->getId())),
                'constraints' => [
                    new File([
                        'maxSize' => '1024k',
                        'mimeTypes' => [
                            'image/jpeg',
                            'image/jpg',
                            'image/png'
                        ],
                        'mimeTypesMessage' => 'Veuillez renseigner une image de type (png, jpg, jpeg).',
                        'maxSizeMessage' => 'Veuillez renseigner une image plus petite (max: 1024 kilobyte). '
                    ])
                ]
            ]);
        });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Image::class
        ]);
    }
}