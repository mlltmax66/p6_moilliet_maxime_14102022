<?php

namespace App\Form;

use App\Entity\Comment;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CommentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('content', TextareaType::class, [
            'attr' => [
                'rows' => '6',
                'class' => 'input'
            ],
            'label' => 'Message',
            'label_attr' => [
                'class' => 'label text-lg'
            ]
        ])->add('submit', SubmitType::class, [
            'attr' => [
                'class' => 'btn btn-primary w-full',
            ],
            'label' => 'Envoyer',
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => Comment::class,
            ]
        );
    }
}