<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\UX\Dropzone\Form\DropzoneType;

class RegisterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, [
                'attr' => [
                    'class' => 'input',
                ],
                'label_attr' => [
                    'class' => 'label'
                ],
                'label' => 'Adresse mail',
            ])
            ->add('username', TextType::class, [
                'attr' => [
                    'class' => 'input',
                ],
                'label_attr' => [
                    'class' => 'label'
                ],
                'label' => 'Votre nom d\'utilisateur',
            ])
            ->add('thumbnail', DropzoneType::class, [
                'attr' => [
                    'placeholder' => 'Glisser ou déposer une image',
                    'class' => 'bg-white'
                ],
                'label' => 'Photo de profil',
                'label_attr' => [
                    'class' => 'label'
                ],
                'mapped' => false,
                'required' => true,
                'constraints' => [
                    new File([
                        'maxSize' => '1024k',
                        'mimeTypes' => [
                            'image/png',
                            'image/jpg',
                            'image/jpeg',
                        ],
                        'mimeTypesMessage' => 'Veuillez renseigner une image png / jpg / jpeg.',
                    ])
                ]
            ])
            ->add('plainPassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'invalid_message' => 'Les deux mots de passes doivent être identiques.',
                'options' => ['attr' => ['class' => 'input'], 'label_attr' => ['class' => 'label']],
                'required' => true,
                'first_options' => ['label' => 'Mot de passe'],
                'second_options' => ['label' => 'Confirmer le mot de passe'],
            ])
            ->add('submit', SubmitType::class, [
                'attr' => [
                    'class' => 'btn-primary mt-2 w-full',
                ],
                'label' => 'S\'inscrire'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => User::class,
            ]
        );
    }
}
