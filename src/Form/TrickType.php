<?php

namespace App\Form;

use App\Entity\Category;
use App\Entity\Trick;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\UX\Dropzone\Form\DropzoneType;

class TrickType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $trick = $options['data'] ?? null;
        $isEdit = $trick && $trick->getId();

        $builder
            ->add('title', TextType::class, [
                'attr' => [
                    'class' => 'input'
                ],
                'label' => 'Nom du trick',
                'label_attr' => [
                    'class' => 'label'
                ]
            ])
            ->add('content', TextareaType::class, [
                'attr' => [
                    'rows' => '6',
                    'class' => 'input'
                ],
                'label' => 'Contenu',
                'label_attr' => [
                    'class' => 'label'
                ]
            ])
            ->add('mainImage', DropzoneType::class, [
                'attr' => [
                    'placeholder' => 'Glisser ou déposer une image',
                    'class' => 'bg-white'
                ],
                'label' => 'Image',
                'label_attr' => [
                    'class' => 'label'
                ],
                'mapped' => false,
                'required' => !$isEdit,
                'constraints' => [
                    new File([
                        'maxSize' => '1024k',
                        'mimeTypes' => [
                            'image/jpeg',
                            'image/jpg',
                            'image/png'
                        ],
                        'mimeTypesMessage' => 'Veuillez renseigner une image de type (png, jpg, jpeg).',
                        'maxSizeMessage' => 'Veuillez renseigner une image plus petite (max: 1024 kilobyte). '
                    ])
                ]
            ])
            ->add('images', CollectionType::class, [
                'entry_type' => ImageType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'error_bubbling' => false,
            ])
            ->add('videos', CollectionType::class, [
                'entry_type' => VideoType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'error_bubbling' => false,
            ])
            ->add('category', EntityType::class, [
                'class' => Category::class,
                'choice_label' => 'name',
                'choice_attr' => [
                    'class' => 'input'
                ],
                'attr' => [
                    'class' => 'input pr-16'
                ],
                'label' => 'Catégorie',
                'label_attr' => [
                    'class' => 'label'
                ]
            ])
            ->add('submit', SubmitType::class, [
                'attr' => [
                    'class' => 'btn-primary mt-2 w-full'
                ],
                'label' => !$isEdit ? 'Publier' : 'Modifier'
            ]);
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Trick::class
        ]);
    }
}