<?php

namespace App\EntityListener;

use App\Entity\Trick;
use App\Service\SlugifyService;

class TrickListener
{
    public function __construct(private readonly SlugifyService $slugifyService)
    {
    }

    public function prePersist(Trick $trick): void
    {
        $slug = $this->slugifyService->slugify($trick->getTitle());
        $trick->setSlug($slug);
    }

    public function preUpdate(Trick $trick): void
    {
        $slug = $this->slugifyService->slugify($trick->getTitle());
        $trick->setSlug($slug);
    }
}