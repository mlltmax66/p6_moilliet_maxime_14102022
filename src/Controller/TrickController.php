<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\Trick;
use App\Form\CommentType;
use App\Form\TrickType;
use App\Service\CommentService;
use App\Service\TrickService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TrickController extends AbstractController
{
    #[Route('ajouter-un-trick', name: 'trick_create', methods: ['GET', 'POST'])]
    public function create(Request $request, TrickService $trickService): Response
    {
        $trick = new Trick();
        $form = $this->createForm(TrickType::class, $trick)->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $trickService->storeNewTrick($trick, $form, $this->getUser());

            return $this->redirectToRoute('home');
        }

        return $this->renderForm('trick/create.html.twig', ['form' => $form]);
    }

    #[Route('trick/{slug}', name: 'trick_show', methods: ['GET', 'POST'])]
    public function show(Trick $trick, Request $request, CommentService $commentService): Response
    {
        $comment = new Comment();
        $formComment = $this->createForm(CommentType::class, $comment)->handleRequest($request);

        if ($formComment->isSubmitted() && $formComment->isValid()) {
            $commentService->storeNewComment($trick, $comment, $this->getUser());

            $this->addFlash('success', 'Votre commentaire a bien été enregistré !');

            return $this->redirectToRoute('trick_show', ['slug' => $trick->getSlug()]);
        }
        return $this->renderForm('trick/show.html.twig', ['trick' => $trick, 'formComment' => $formComment]);
    }

    #[Route('modifier-un-trick/{slug}', name: 'trick_update', methods: ['GET', 'POST'])]
    public function update(Trick $trick, Request $request, TrickService $trickService): Response
    {
        $form = $this->createForm(TrickType::class, $trick)->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $trickService->updateTrick($trick, $form, $this->getUser());

            return $this->redirectToRoute('home');
        }

        return $this->renderForm('trick/update.html.twig', ['form' => $form, 'trick' => $trick]);
    }

    #[Route('supprimer-un-trick/{id}', name: 'trick_delete', methods: ['POST'])]
    public function delete(Trick $trick, EntityManagerInterface $manager): Response
    {
        $manager->remove($trick);
        $manager->flush();

        $this->addFlash('success', 'Votre trick a bien été supprimé !');

        return $this->redirectToRoute('home');
    }
}