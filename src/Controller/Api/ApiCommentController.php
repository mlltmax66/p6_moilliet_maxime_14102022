<?php

namespace App\Controller\Api;

use App\Repository\CommentRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ApiCommentController extends AbstractController
{
    /**
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    #[Route('/api/comments/{trickId}/{offset}', methods: ['GET'])]
    public function getComments(int $trickId, int $offset, CommentRepository $repository): Response
    {
        return $this->json([
            'comments' => $repository->getComments($trickId, $offset),
            'total' => $repository->getCountComments($trickId)
        ]);
    }
}