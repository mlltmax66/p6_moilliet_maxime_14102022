<?php

namespace App\Controller\Api;

use App\Entity\Trick;
use App\Repository\ImageRepository;
use App\Repository\VideoRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class ApiTrickController extends AbstractController
{
    #[Route('/api/tricks-images-videos/{slug}', methods: ['GET'])]
    public function getImagesVideosTricks(
        Trick           $trick,
        ImageRepository $imageRepository,
        VideoRepository $videoRepository
    ): JsonResponse
    {
        return $this->json([
            'images' => $imageRepository->getImagesTrick($trick),
            'videos' => $videoRepository->getTrickVideos($trick)
        ]);
    }
}