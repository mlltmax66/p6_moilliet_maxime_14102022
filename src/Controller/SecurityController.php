<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\ForgotPasswordType;
use App\Form\RegisterType;
use App\Form\ResetPasswordType;
use App\Repository\UserRepository;
use App\Service\FileUploader;
use App\Service\GenerateUserToken;
use App\Service\MailerService;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    #[Route('/connexion', name: 'security_login', methods: ['GET', 'POST'])]
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        return $this->render('security/login.html.twig', [
            'lastUsername' => $authenticationUtils->getLastUsername(),
            'error' => $authenticationUtils->getLastAuthenticationError()
        ]);
    }

    /**
     * @throws Exception
     */
    #[Route('/deconnexion', name: 'security_logout')]
    public function logout(): never
    {
        throw new Exception('Don\'t forget to activate logout in security.yaml');
    }

    /**
     * @throws TransportExceptionInterface
     */
    #[Route('/mot-de-passe-oublie', name: 'security_forgot_password', methods: ['GET', 'POST'])]
    public function forgotPassword(
        Request           $request,
        UserRepository    $repository,
        GenerateUserToken $generateUserToken,
        MailerService     $mailerService
    ): Response
    {
        $form = $this->createForm(ForgotPasswordType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $repository->findOneBy(['username' => $form->get('username')->getData()]);

            if ($user) {
                $user = $generateUserToken->execute($user);
                $mailerService->resetPassword($user);
            }

            $this->addFlash('success', 'Un mail vous a été envoyé');
        }

        return $this->render('security/forgot_password.html.twig', ['form' => $form->createView()]);
    }

    #[Route('/reinitialiser-mot-de-passe/{reset_token}', name: 'security_reset_password', methods: ['GET', 'POST'])]
    public function resetPassword(User $user, Request $request, EntityManagerInterface $manager): Response
    {
        $form = $this->createForm(ResetPasswordType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setResetToken(null);
            $manager->flush();

            $this->addFlash('success', 'Votre mot de passe a bien été modifié !');

            return $this->redirectToRoute('security_login');
        }

        return $this->render('security/reset_password.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @throws TransportExceptionInterface
     */
    #[Route('/inscription', name: 'security_register', methods: ['GET', 'POST'])]
    public function register(
        Request                 $request,
        EntityManagerInterface  $manager,
        FileUploader            $fileUploader,
        TokenGeneratorInterface $tokenGenerator,
        MailerService           $mailerService): Response
    {
        $user = new User();

        $form = $this->createForm(RegisterType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $thumbnail = $form->get('thumbnail')->getData();
            $thumbnailPath = $fileUploader->upload($thumbnail, 'users');

            $user->setActivationToken($tokenGenerator->generateToken());

            $user->setThumbnail($thumbnailPath);
            $manager->persist($user);
            $manager->flush();

            $mailerService->verifyEmail($user);

            $this->addFlash('success', 'Votre compte à bien été créé, un email de confirmation vient de vous être envoyé !');

            return $this->redirectToRoute('security_login');
        }

        return $this->render('security/register.html.twig', ['form' => $form->createView()]);
    }

    #[Route('/activation-account/{token}', name: 'security_activate')]
    public function activateAccount(string $token, UserRepository $repository, EntityManagerInterface $manager): Response
    {
        $user = $repository->findOneBy(['activationToken' => $token]);

        if (!$user) {
            $this->addFlash('danger', 'Problème d\'identification !');

            return $this->redirectToRoute('security_login');
        }

        $user->setActivationToken(null);
        $user->setRoles(['ROLE_USER_VERIFY']);
        $manager->persist($user);
        $manager->flush();

        $this->addFlash('success', 'Votre compte a été activé !');

        return $this->redirectToRoute('security_login');
    }

    /**
     * @throws TransportExceptionInterface
     */
    #[Route('/resend-email-activate-account', name: 'security_resend_email_activate_account', methods: ['GET'])]
    public function resendEmailActivateAccount(
        TokenGeneratorInterface $tokenGenerator,
        EntityManagerInterface $manager,
        MailerService $mailerService
    ): Response
    {
        $user = $this->getUser();
        $user->setActivationToken($tokenGenerator->generateToken());
        $manager->persist($user);
        $manager->flush();

        $mailerService->verifyEmail($user);

        $this->addFlash('success', 'Un email de confirmation vient de vous être envoyé !');

        return $this->redirectToRoute('security_login');
    }
}