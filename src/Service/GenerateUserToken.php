<?php

namespace App\Service;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;

class GenerateUserToken
{
    public function __construct(
        readonly private TokenGeneratorInterface $tokenGenerator,
        readonly private EntityManagerInterface  $entityManager,
    )
    {
    }

    public function execute(User $user): User
    {
        $token = $this->tokenGenerator->generateToken();
        $user->setResetToken($token);

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return $user;
    }
}