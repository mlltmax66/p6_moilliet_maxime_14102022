<?php

namespace App\Service;

use App\Entity\Comment;
use App\Entity\Trick;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class CommentService
{
    public function __construct(private readonly EntityManagerInterface $manager)
    {
    }

    public function storeNewComment(Trick $trick, Comment $comment, User|UserInterface $user): void
    {
        $comment->setTrick($trick)->setUser($user);

        $this->manager->persist($comment);
        $this->manager->flush();
    }
}