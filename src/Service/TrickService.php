<?php

namespace App\Service;

use App\Entity\Trick;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class TrickService
{
    public function __construct(
        private readonly FileUploader           $uploader,
        private readonly EntityManagerInterface $manager
    )
    {
    }

    public function storeNewTrick(Trick $trick, FormInterface $form, User|UserInterface $user): void
    {
        $mainImageName = $this->uploader->upload($form->get('mainImage')->getData(), 'tricks');
        $trick->setMainImage($mainImageName);

        foreach ($trick->getImages() as $image) {
            $imageName = $this->uploader->upload($image->getFile(), 'tricks');
            $image->setFilename($imageName);
            $this->manager->persist($image);
        }

        foreach ($trick->getVideos() as $video) {
            $video->setSource($video->getSource());
            $this->manager->persist($video);
        }

        $trick->setUser($user);
        $this->manager->persist($trick);
        $this->manager->flush();
    }

    public function updateTrick(Trick $trick, FormInterface $form, User|UserInterface $user): void
    {
        if ($form->get('mainImage')->getData()) {
            $mainImageName = $this->uploader->upload($form->get('mainImage')->getData(), 'tricks');
            $trick->setMainImage($mainImageName);
        }

        foreach ($trick->getImages() as $image) {
            if ($image->getFile()) {
                $imageName = $this->uploader->upload($image->getFile(), 'tricks');
                $image->setFilename($imageName);
                $this->manager->persist($image);
            }
        }

        foreach ($trick->getVideos() as $video) {
            $video->setSource($video->getSource());
            $this->manager->persist($video);
        }

        $trick->setUser($user);
        $this->manager->persist($trick);
        $this->manager->flush();
    }
}