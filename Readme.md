# SnowTricks

[![Codacy Badge](https://app.codacy.com/project/badge/Grade/6a0b696a198840b8b2e156263cbd90bb)](https://app.codacy.com/gl/mlltmax66/p6_moilliet_maxime_14102022/dashboard?utm_source=gl&utm_medium=referral&utm_content=&utm_campaign=Badge_grade)

Création d'un site communautaire de partage de figures de snowboard.

## Prérequis

PHP version >= 8.1 et npm ou yarn.

## Installation

1. Clonez ou téléchargez le repository :
```
    git clone https://gitlab.com/mlltmax66/p6_moilliet_maxime_14102022
```
2. Configurez vos variables d'environnement tel que la connexion à la base de données/serveur SMTP/adresse mail dans le fichier `.env.local` :

3. Installez les dépendances du projet avec [Composer](https://getcomposer.org/download/) :
```
    composer install
```
4. Installez les dépendances du projet avec [Yarn](https://classic.yarnpkg.com/en/docs/install) :
```
    yarn install ou npm install
```
5. Créez un build d'assets (grâce à Webpack Encore) avec [Yarn](https://classic.yarnpkg.com/en/docs/install) :
```
    yarn dev or npm run dev
```
6. Créez la base de données si elle n'existe pas déjà :
```
    php bin/console d:d:c
```
7. Lancez les migrations :
```
    php bin/console d:m:m
```

8. (Optionnel) Lancez les fixtures :
```
    php bin/console d:f:l
```
9. Lancement du serveur :
```
    php bin/console server:run ou php -S localhost:8000 -t public
```
9. Dirigez vous sur http://127.0.0.1:8000.